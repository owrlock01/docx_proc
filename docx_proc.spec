# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['src\\docx_proc.py'],
             pathex=['C:\\Users\\Lenovo\\PycharmProjects\\docx_proc'],
             binaries=[('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/ruword2tags/ruword2tags.db', './ruword2tags/'),
                       ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/rupostagger/rupostagger.model', './rupostagger/')],
             datas=[('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/rulemma/rulemma.dat', './rulemma/'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/rutokenizer/rucorpustokenizer.dat', './rutokenizer/'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/rutokenizer/rutokenizer.dat', './rutokenizer/'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/rupostagger/rupostagger.config', './rupostagger/'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/ruword2tags/ruflexer.dat', './ruword2tags/'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/ruword2tags/ruword2tags.dat', './ruword2tags/'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc/venv/lib/python3.6/site-packages/tkfilebrowser/images/', './tkfilebrowser/images/'),
		    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc\\src\\phrases', '.'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc\\src\\words', '.'),
                    ('C:\\Users\\Lenovo\\PycharmProjects\\docx_proc\\src\\stopwords', '.')],
             hiddenimports=['pycrfsuite._dumpparser', 'pycrfsuite._logparser'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='docx_proc',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )
