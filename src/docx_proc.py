import re
from string import punctuation

import sys
import os

from apply import apply
from docx import Document

import rutokenizer
import rupostagger
import rulemma

import tkinter as tk
from tkinter import BOTH, messagebox, W, X, FLAT, LEFT, YES, RAISED, FALSE, VERTICAL, Y, END
from tkinter.ttk import *

from tkfilebrowser import askopendirname

from glob import glob


class MultiListbox(Frame):
    def __init__(self, master, lists):
        Frame.__init__(self, master)
        self.lists = []
        for l,w in lists:
            frame = Frame(self); frame.pack(side=LEFT, expand=YES, fill=BOTH)
            Label(frame, text=l, borderwidth=1, relief=RAISED).pack(fill=X)
            lb = tk.Listbox(frame, width=w, borderwidth=0, selectborderwidth=0,
                            relief=FLAT, exportselection=FALSE)
            lb.pack(expand=YES, fill=BOTH)
            self.lists.append(lb)
            lb.bind('<B1-Motion>', lambda e, s=self: s._select(e.y))
            lb.bind('<Button-1>', lambda e, s=self: s._select(e.y))
            lb.bind('<Leave>', lambda e: 'break')
            lb.bind('<B2-Motion>', lambda e, s=self: s._b2motion(e.x, e.y))
            lb.bind('<Button-2>', lambda e, s=self: s._button2(e.x, e.y))
        frame = Frame(self); frame.pack(side=LEFT, fill=Y)
        Label(frame, borderwidth=1, relief=RAISED).pack(fill=X)
        sb = Scrollbar(frame, orient=VERTICAL, command=self._scroll)
        sb.pack(expand=YES, fill=Y)
        self.lists[0]['yscrollcommand']=sb.set

    def _select(self, y):
        row = self.lists[0].nearest(y)
        self.selection_clear(0, END)
        self.selection_set(row)
        return 'break'

    def _button2(self, x, y):
        for l in self.lists: l.scan_mark(x, y)
        return 'break'

    def _b2motion(self, x, y):
        for l in self.lists: l.scan_dragto(x, y)
        return 'break'

    def _scroll(self, *args):
        for l in self.lists:
            apply(l.yview, args)

    def curselection(self):
        return self.lists[0].curselection(  )

    def delete(self, first, last=None):
        for l in self.lists:
            l.delete(first, last)

    def get(self, first, last=None):
        result = []
        for l in self.lists:
            result.append(l.get(first,last))
        if last:
            return apply(map, [None] + result)
        return result

    def index(self, index):
        self.lists[0].index(index)

    def insert(self, index, *elements):
        for e in elements:
            i = 0
            for l in self.lists:
                l.insert(index, e[i])
                i = i + 1

    def size(self):
        return self.lists[0].size(  )

    def see(self, index):
        for l in self.lists:
            l.see(index)

    def selection_anchor(self, index):
        for l in self.lists:
            l.selection_anchor(index)

    def selection_clear(self, first, last=None):
        for l in self.lists:
            l.selection_clear(first, last)

    def selection_includes(self, index):
        return self.lists[0].selection_includes(index)

    def selection_set(self, first, last=None):
        for l in self.lists:
            l.selection_set(first, last)


def open_dir_dialog(dirpath_btn):
    path = askopendirname(parent=master)
    if path:
        docx_list = glob(path + "/*.docx")
        if not docx_list:
            messagebox.showinfo(title=None, message="Указанная директория не содержит DOCX-файлов.")
        else:
            dirpath_btn.configure(text="Идет обработка файлов. Подождите...", command=do_nothing)
            global result
            result = init_processing(docx_list)
            show_result_frame()

def get_text(docxf):
    doc = Document(docxf)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)


def show_spec(lemmas, spec_words, stopwords):
    lemma_count = {}

    for word, tags, lemma, *_ in lemmas:
        if lemma not in stopwords and \
                lemma in spec_words:
            if lemma in lemma_count:
                lemma_count[lemma][0] += 1
                lemma_count[lemma][1] = lemma_count[lemma][0] / len(lemmas) * 100
            else:
                lemma_count[lemma] = [1, 1 / len(lemmas) * 100]

    return lemma_count


def show_all(lemmas, stopwords):
    all_count = {}

    for word, tags, lemma, *_ in lemmas:
        if lemma not in stopwords:
            if lemma in all_count:
                all_count[lemma][0] += 1
                all_count[lemma][1] = all_count[lemma][0] / len(lemmas) * 100
            else:
                all_count[lemma] = [1, 1 / len(lemmas) * 100]

    return all_count


def show_phrases(phrases, text):
    phrase_count = {}

    for phrase in phrases:
        if phrase in text.lower():
            phrase_count[phrase] = 1 if phrase not in phrase_count else phrase_count[phrase] + 1

    return phrase_count


def process(file):
    text = get_text(file)

    text = re.sub(r'[0-9]+', '', text)

    with open(resource_path('stopwords'), 'r') as f:
        russian_stopwords = f.read().split('\n')

    tokens = tokenizer.tokenize(text)
    tags = tagger.tag(tokens)
    lemmas = lemmatizer.lemmatize(tags)

    punctuation_ext = punctuation[:]
    punctuation_ext += "«»–"

    with open(resource_path('words'), 'r') as f:
        words = f.read().split('\n')

    with open(resource_path('phrases'), 'r') as f:
        phrases = f.read().split('\n')

    words = [w.encode('Windows-1251').decode('UTF-8') for w in words]
    phrases = [p.encode('Windows-1251').decode('UTF-8') for p in phrases]

    lemmas = [lemma for lemma in lemmas if lemma[2].strip() not in punctuation_ext]
    result = {}
    result['spec'] = show_spec(lemmas, words, russian_stopwords)
    result['all'] = show_all(lemmas, russian_stopwords)
    result['phr'] = show_phrases(phrases, text)
    result['total_all'] = sum([result['all'][key][0] for key in result['all']])
    result['total_spec'] = sum([result['spec'][key][0] for key in result['spec']])
    return result


def do_nothing():
    pass


def show_selection_frame():
    master.resizable(0, 0)
    master.geometry("%dx%d+%d+%d" % (wwidth_f1, wheight_f1, x_coor, y_coor))
    result_frame.pack_forget()

    # SELECTION FRAME
    select_frame.pack(fill=BOTH, expand=True)
    # +++ dirpath button
    dirpath_button = Button(select_frame, text='Путь к директории', command=lambda: open_dir_dialog(dirpath_button))
    dirpath_button.pack(anchor="s", side="bottom", pady=(0, 10))
    # +++ dirpath label
    dirpath_label = Label(select_frame, text="Укажите путь к директории c документами.")
    dirpath_label.pack(anchor="n", side="top", pady=(10, 0))


def show_result_frame():
    master.resizable(1, 1)
    master.geometry("%dx%d+%d+%d" % (wwidth_f2, wheight_f2, x_coor, y_coor))
    select_frame.pack_forget()

    # RESULT FRAME
    global wcvlabel, stwcvlabel
    result_frame.pack(fill=BOTH, side="left")
    # +++ list label
    list_label = Label(result_frame, text="Выберите документ:")
    list_label.pack(anchor="nw", pady=10, padx=10)
    # +++ listbox
    filebox = tk.Listbox(result_frame)
    filebox.pack(anchor="nw", fill=X, padx=10, pady=(0, 10))
    # ++++++ word count subframe
    wcsubframe = Frame(result_frame)
    wcsubframe.pack(fill=X)
    # +++ word count label
    wclabel = Label(wcsubframe, text="Слов в документе:")
    wclabel.pack(anchor="nw", side="left", padx=10, pady=(0, 10))
    # +++ word count label value
    wcvlabel = Label(wcsubframe, text="0")
    wcvlabel.pack(anchor="nw", side="right", padx=10, pady=(0, 10))
    # ++++++ sec topic word count subframe
    stwcsubframe = Frame(result_frame)
    stwcsubframe.pack(fill=X)
    # +++ security topic word count label
    stwclabel = Label(stwcsubframe, text="Соответствующих специальности:")
    stwclabel.pack(anchor="nw", side="left", padx=10, pady=(0, 10))
    # +++ security topic word count label value
    stwcvlabel = Label(stwcsubframe, text="0")
    stwcvlabel.pack(anchor="nw", side="right", padx=10, pady=(0, 10))

    global mlb_spec, mlb_phr, mlb_all, notebook
    notebook = Notebook(master)
    notebook.pack(fill=BOTH, side="right", expand=True)
    mlb_spec = MultiListbox(notebook, (('Объект', 20), ('Частота [абс]', 10), ('Частота [отн]', 10)))
    mlb_spec.pack(fill=BOTH)
    mlb_phr = MultiListbox(notebook, (('Объект', 40), ('Частота [абс]', 20)))
    mlb_phr.pack(fill=BOTH)
    mlb_all = MultiListbox(notebook, (('Объект', 20), ('Частота [абс]', 10), ('Частота [отн]', 10)))
    mlb_all.pack(fill=BOTH)
    notebook.add(mlb_spec, text="Спец. слова")
    notebook.add(mlb_phr, text="Спец. фразы")
    notebook.add(mlb_all, text="Все слова")

    global result

    if result:
        for filename in result.keys():
            filebox.insert(END, filename)
        filebox.bind('<<ListboxSelect>>', onselect)


def onselect(event):
    global result, mlb_spec, mlb_phr, mlb_all, wcvlabel, stwcvlabel

    mlb_all.delete(0, END)
    mlb_spec.delete(0, END)
    mlb_phr.delete(0, END)

    w = event.widget
    index = int(w.curselection()[0])
    value = w.get(index)
    data = result[value]

    if mlb_spec and mlb_all and mlb_phr and wcvlabel and stwcvlabel:
        for k, v in sorted(data['spec'].items(), key=lambda x: x[1][0], reverse=True):
            mlb_spec.insert(END, (k, v[0], v[1]))
        for k, v in sorted(data['all'].items(), key=lambda x: x[1][0], reverse=True):
            mlb_all.insert(END, (k, v[0], v[1]))
        for k, v in sorted(data['phr'].items(), key=lambda x: x[1], reverse=True):
            mlb_phr.insert(END, (k, v))
        wcvlabel.configure(text=data['total_all'])
        stwcvlabel.configure(text=data['total_spec'])


def init_processing(docx_list):
    processed = {}
    for docx in docx_list:
        try:
            processed[docx.split('\\')[-1]] = process(docx)
        except Exception as ex:
            print(ex)
    return processed


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

# GLOBAL
notebook = None
mlb_spec = None
mlb_phr = None
mlb_all = None
wcvlabel = None
stwcvlabel = None
result = {}

if __name__ == "__main__":
    master = tk.Tk()
    master.title("Проверка")

    wwidth_f1 = 310
    wheight_f1 = 90
    wwidth_f2 = 800
    wheight_f2 = 500

    scrwidth = master.winfo_screenwidth()
    scrheight = master.winfo_screenheight()

    x_coor = scrwidth / 2 - wwidth_f1 / 2
    y_coor = scrheight / 2 - wheight_f1 / 2

    master.geometry("+%d+%d" % (x_coor, y_coor))
    master.resizable(0, 0)

    select_frame = Frame(master)
    result_frame = Frame(master)

    show_selection_frame()

    lemmatizer = rulemma.Lemmatizer()
    lemmatizer.load()

    tokenizer = rutokenizer.Tokenizer()
    tokenizer.load()

    tagger = rupostagger.RuPosTagger()
    tagger.load()

    master.mainloop()
